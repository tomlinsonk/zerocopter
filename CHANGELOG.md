# Change Log
All notable changes to this project will be documented in this file.

## Unversioned 
### Added
- Flight logging system
- Abort procedure 

### Changed
- Improved sensor error handling

## 0.3 - 2016-10-04
### Added
- LED control in three modes: static, blink, burst
- Input from switch engages FSW
- ESC arming

### Changed
- Improved IMU multithreading

### Removed
- Took out temporary main.cpp control file

## 0.2 - 2016-09-28
### Changed
- Migrated from wiringPi to pigpio

## 0.1 - 2016-09-28
### Added
- Data in from gyroscope, accelerometer, magnetometer, barometer, and sonar rangefinder
- Gyro and accel sensor fusion
- Bar data filtering
- IMU class to control the sensors
- Sensor threads run asynchronously