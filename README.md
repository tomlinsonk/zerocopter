This is a WIP. I am building an autonomous quadcopter controlled by a Raspberry Pi Zero,
and this is the FSW. The sensor module is complete! Coming soon: PID stabilization and flight.

# Current features:
* Read data from gyroscope, accelerometer, magnetometer, sonar rangefinder, and barometer over I2C
* Barometer data smoothed using exponential moving average filter
* Gyro and accel data combined with complimentary filter to get pitch/roll
* Magnetometer data is corrected by pitch/roll values to get compass heading
* Sensor data collection code is run asynchronously
* LED is used to display current status
* A switch is used to engage the Zerocopter after boot
* ESCs are armed at FSW engage

# Coming soon:
* PID controllers for motors
* Flying through the air
* Simple mission execution (eg: takeoff, hover at 3m, spin 180deg, hover at 1m, land)

# Hardware:
The Zerocopter is built from the following parts:
* Raspberry Pi Zero v1.3
* LHI RX-220 frame (clone of the Lumenier QAV-R)
* HobbyMate 2204 2300kv motors
* HobbyMate 12A ESCs
* Matek PDB
* Turnigy 1300mAh 3s LiPo battery
* Raspberry Pi Camera Module v2
* Maxbotix I2CXL-MaxSonar-EZ4 rangefinder
* Adafruit 10-DOF IMU 
 * L3GD20H gyroscope
 * LSM303 accelerometer and magnetometer
 * BMP180 barometer
* Edimax EW-7811Un wifi dongle
* A switch, an LED, lots of zipties, and some tape.
