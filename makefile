CC = g++
CFLAGS = -Wall -g -std=c++14
LFLAGS = -pthread -lrt -lpigpio


MODULES = core sensors io
SRC_DIR = $(addprefix src/,$(MODULES))
INCL = $(foreach d,$(SRC_DIR),-I$d) -Ilib
TARGET = zerocopter
SRC = $(foreach sdir,$(SRC_DIR),$(wildcard $(sdir)/*.cpp))
OBJ = $(patsubst src/%.cpp, obj/%.o,$(SRC))

$(TARGET): $(OBJ)
	$(CC) $(CFLAGS) $(LFLAGS) $(OBJ) -o $(TARGET)

obj/%.o: src/%.cpp
	$(CC) $(CFLAGS) $(INCL) -c $< -o $@

clean:
	find . -exec touch {} \;
	rm -f $(OBJ)
	rm -f $(TARGET)

depend: .depend

.depend: $(SRC)
	$(CC) $(CFLAGS) $(INCL) -MM $^ > ./.depend;

include .depend
