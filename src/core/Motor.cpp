/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-09-30T21:50:31-05:00
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   Kiran Tomlinson
* @Last modified time: 2016-10-04
*/



#include "Motor.h"

#include <pigpio.h>

#include <iostream>

using namespace std;

/**
 * Constructor for motor
 * @param pin the signal wire for the motor
 */
Motor::Motor(int pinNumber) {
    pin = pinNumber;
    gpioSetPWMfrequency(pin, 100);
    gpioSetPWMrange(pin, 10000);
    throttle = 0;
}

/**
 * Set the throttle on this motor, within constraints
 * @param throttle PWM value from 1000-2000 us
 */
void Motor::setThrottle(int newThrottle) {
    if (newThrottle < 1000) {
        newThrottle = 1000;
    } else if (newThrottle > 2000) {
        newThrottle = 2000;
    }
    
    throttle = newThrottle;
    gpioPWM(pin, throttle);
}

/**
 * Accessor for the throttle setting
 * @return throttle
 */
int Motor::getThrottle() {
    return throttle;
}

/**
 * Arm the motor
 */
void Motor::arm() {
    for (int i = 1000; i < 1100; i++) {
        setThrottle(i);
        gpioDelay(10000);
    }
    for (int i = 1100; i >= 1000; i--) {
        setThrottle(i);
        gpioDelay(10000);
    }
}