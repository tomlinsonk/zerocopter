/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-10-01
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   Kiran Tomlinson
* @Last modified time: 2016-10-04
*/



#include "Zerocopter.h"
#include "FlightLog.h"

#include <pigpio.h>
#include <plog/Log.h>

#include <iostream>
#include <thread>

using namespace std;


/**
 * Entry point for the Zerocopter FSW.
 */
int main() {
    createFlightLog();
    gpioInitialise();
    Zerocopter zerocopter = Zerocopter();
    zerocopter.init();
    zerocopter.fly();
    zerocopter.cleanup();
    gpioTerminate();
    
    LOG_INFO << "done";
    return 0;
}

/**
 * Constructor. Everything important here happens in the .h file
 */
Zerocopter::Zerocopter() {}

/**
 * Get ready to fly. Enter standby mode.
 */
void Zerocopter::init() {
    // Wait for engage sequence. Infinite loop until sequence occurs.
    // Enage sequence: on > off > on within 1 second.
    // Refresh at 100Hz.
    LOG_INFO << "standby for launch";
    led.blink(300000);
    
    bool armed, engaged;
    int secs, mics;
    int newSecs, newMics;
    while (true) {
        armed = false;
        engaged = false;
        
        if (!toggle.getValue()) {
            armed = true;
            gpioTime(PI_TIME_RELATIVE, &secs, &mics);
        }
        
        if (armed) {
            while (true) {
                if (toggle.getValue()) {
                    gpioTime(PI_TIME_RELATIVE, &newSecs, &newMics);
                    if (newSecs - secs == 0 || (newSecs - secs == 1 && mics > 500000 && newMics < 500000)) {
                        engaged = true;
                        LOG_INFO << "engage sequence detected";
                    }
                    break;
                }
                gpioDelay(10000);
            }
        }
        
        if (engaged) {
            break;
        }
        
        gpioDelay(10000);
    }
    
    // Engage! Arm motors!
    LOG_INFO << "arm motors";
    led.set(true);
    thread arm1(&Motor::arm, motor1);
    thread arm2(&Motor::arm, motor2);
    thread arm3(&Motor::arm, motor3);
    thread arm4(&Motor::arm, motor4);
    arm1.join();
    arm2.join();
    arm3.join(); 
    arm4.join();
}


/**
 * TODO: Make this work
 */
void Zerocopter::fly() {
    LOG_INFO << "liftoff";
}

/**
 * Ask modules to clean up and give them some extra time.
 */
void Zerocopter::cleanup() {
    LOG_INFO << "cleaning up";
    imu.cleanup();
    led.cleanup();
    gpioDelay(200000);
}

/**
 * Very bad. Land now, sensor malfunction. ABANDON SHIP!
 */
void Zerocopter::abort() {
    LOG_FATAL << "ABORT, ABORT, ABORT";
    led.burst(3);
    
    int secs, mics;
    int newSecs, newMics;
    gpioTime(PI_TIME_RELATIVE, &secs, &mics);
    gpioTime(PI_TIME_RELATIVE, &newSecs, &newMics);
    
    int motorThrottle1 = motor1.getThrottle();
    int motorThrottle2 = motor2.getThrottle();
    int motorThrottle3 = motor3.getThrottle();
    int motorThrottle4 = motor4.getThrottle();
    
    while (newSecs - secs < 10) {
        gpioTime(PI_TIME_RELATIVE, &newSecs, &newMics);
        motorThrottle1--;
        motorThrottle2--;
        motorThrottle3--;
        motorThrottle4--;
        
        if (motorThrottle1 < 1100) motorThrottle1 = 1100;
        if (motorThrottle2 < 1100) motorThrottle2 = 1100;
        if (motorThrottle3 < 1100) motorThrottle3 = 1100;
        if (motorThrottle3 < 1100) motorThrottle3 = 1100;
        
        motor1.setThrottle(motorThrottle1);
        motor2.setThrottle(motorThrottle2);
        motor3.setThrottle(motorThrottle3);
        motor4.setThrottle(motorThrottle4);
        
        gpioDelay(10000);
    }
}

