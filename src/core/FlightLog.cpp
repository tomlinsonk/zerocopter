/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-10-01
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   Kiran Tomlinson
* @Last modified time: 2016-10-04
*/

#include <plog/Log.h>

#include <iostream>
#include <ctime>

using namespace std;


/**
 * Create a new .flightlog file and initialize plog
 */
void createFlightLog() {
    time_t rawTime;
    struct tm* timeInfo;
    char buffer[80];

    time(&rawTime);
    timeInfo = localtime(&rawTime);

    strftime(buffer, 80, "%Y-%m-%d_%I-%M-%S", timeInfo);
    string timeString(buffer);
    
    plog::init(plog::verbose, ("/home/pi/projects/zerocopter/log/" + timeString + ".flightlog").c_str(), 1000000, 1);
    
    LOG_INFO << "start logging";
}
