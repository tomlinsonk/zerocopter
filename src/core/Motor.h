/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-09-30
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   Kiran Tomlinson
* @Last modified time: 2016-10-04
*/



#ifndef ZEROCOPTER_MOTOR_H
#define ZEROCOPTER_MOTOR_H

/**
 * The motor class represents a single ESC-motor pair.
 * This code assumes that the ESC has been calibrated to work in the 1000-2000 range.
 */
class Motor {
private:
    int pin;
    int throttle;
public:
    Motor(int pinNumber);
    void setThrottle(int newThrottle);
    int getThrottle();
    void arm();
};

#endif // ZEROCOPTER_MOTOR_H