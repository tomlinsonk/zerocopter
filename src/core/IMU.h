/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-09-30
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   Kiran Tomlinson
* @Last modified time: 2016-10-04
*/
/*
 * See datasheets for more info:
 * Gyro:
 * https://cdn-shop.adafruit.com/datasheets/L3GD20H.pdf
 *
 * Accel/Mag:
 * http://www.st.com/content/ccc/resource/technical/document/datasheet/56/ec/ac/de/28/21/4d/48/DM00027543.pdf/files/DM00027543.pdf/jcr:content/translations/en.DM00027543.pdf
 *
 * Bar:
 * https://www.sparkfun.com/datasheets/Components/General/BST-BMP085-DS000-05.pdf
 *
 * Sonar:
 * http://www.maxbotix.com/documents/I2CXL-MaxSonar-EZ_Datasheet.pdf
 */


#ifndef ZEROCOPTER_IMU_H
#define ZEROCOPTER_IMU_H

#include "Gyro.h"
#include "Accel.h"
#include "Mag.h"
#include "Bar.h"
#include "Sonar.h"



struct Orientation {
    float roll;
    float pitch;
    float heading;
};



class IMU {

private:
    Gyro gyro;
    Accel accel;
    Mag mag;
    Bar bar;
    Sonar sonar;
    
    float altitude;
    Orientation orientation;
    float height;

    bool stopped;
    bool failure;
    
    void updateAltitude();
    void updateOrientation();
    void updateHeading();
    void updateHeight();


public:
    IMU();
    Orientation getOrientation();
    float getAltitude();
    float getHeight();
    void cleanup();
};

#endif //ZEROCOPTER_IMU_H
