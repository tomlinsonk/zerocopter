/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-09-30
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   Kiran Tomlinson
* @Last modified time: 2016-10-04
*/



#include "IMU.h"

#include "plog/Log.h"

#include <cmath>
#include <iostream>
#include <pigpio.h>
#include <thread>

using namespace std;

/********************
 * PUBLIC FUNCTIONS *
 ********************/


/**
 * Constructor. Initialize all devices.
 * @return nothing
 */
IMU::IMU() {
    stopped = false;
    failure = false;
    altitude = 0;
    height = 0;
    orientation.pitch = 0;
    orientation.roll = 0;
    orientation.heading = 0;

    // Set all device modes
    gyro.setRate(GYRO_RATE_100);
    gyro.setRange(GYRO_RANGE_245);
    mag.setRate(MAG_RATE_75);
    mag.setRange(MAG_RANGE_1_3);
    accel.setRate(ACCEL_RATE_100);
    accel.setRange(ACCEL_RANGE_2);
    bar.setMode(BAR_MODE_ULTRA_HIGH_RES);
    
    thread altitudeThread(&IMU::updateAltitude, this);
    altitudeThread.detach();
    
    thread gyroAccelThread(&IMU::updateOrientation, this);
    gyroAccelThread.detach();
     
    thread magThread(&IMU::updateHeading, this);
    magThread.detach();

    thread sonarThread(&IMU::updateHeight, this);
    sonarThread.detach();
}

/**
 * Calculate orientation from accel, gyro, and mag data.
 * @return the roll, pitch, and heading in an Orientation struct
 */
Orientation IMU::getOrientation() {
    return orientation;
}

/**
 * Calculate altitude from bar data.
 * @return the altitude in m
 */
float IMU::getAltitude() {
    return altitude;
}

float IMU::getHeight() {
    return height;
}


/**
 * Infinite loop to read bar data and update altitude every 0.03 secs at BAR_MODE_ULTRA_HIGH_RES.
 * Uses an exponential moving average to decrease noise.
 */
void IMU::updateAltitude() {
    float alpha = 0.1;
    float invAlpha = 1.0 - alpha;
    float seaLevel = 1013.25;
    
    BarData barData = bar.getBarData();
    float pressure = barData.pressure / 100.0;
    float newAltitude = 44330.0 * (1.0 - pow(pressure / seaLevel, 0.1903));
    altitude = newAltitude;
    
    while (!stopped) {
        barData = bar.getBarData();
        pressure = barData.pressure / 100.0;
        newAltitude = 44330.0 * (1.0 - pow(pressure / seaLevel, 0.1903));
        altitude = (alpha * newAltitude) + (invAlpha * altitude);
        
        if (bar.getFailure()) {
            failure = true;
            LOG_ERROR << "bar error detected";
        }
    }
}

/**
 * Infinite loop to read gyro and accel data to get pitch and roll.
 * Uses a complementary filer to increase accuracy
 */
void IMU::updateOrientation() {
    float dt = 0.01;
    float accelRoll;
    float accelPitch;
    GyroData gyroData;
    AccelData accelData;
    
    while (!stopped) {
        gpioDelay(10000);
        gyroData = gyro.getData();
        orientation.pitch += gyroData.x * dt;
        orientation.roll += gyroData.y * dt;
        
        accelData = accel.getData();
        accelPitch = (float)atan2(accelData.y, accelData.z);
        accelRoll = (float)atan(-accelData.x / (accelData.y * sin(accelPitch) + accelData.z * cos(accelPitch)));
        
        orientation.roll = orientation.roll * 0.95 + accelRoll * 0.05;
        orientation.pitch = orientation.pitch * 0.95 + accelPitch * 0.05;
        
        if (gyro.getFailure()) {
            failure = true;
            LOG_ERROR << "gyro error detected";
        }
        
        if (accel.getFailure()) {
            failure = true;
            LOG_ERROR << "accel error detected";
        }
    }
}


/**
 * Infinite loop to read mag data.
 * Compensate for pitch/roll
 */
void IMU::updateHeading() {
    MagData magData;
    
    while (!stopped) {
        gpioDelay(14000);
        magData = mag.getData();
        orientation.heading = (float)atan2(magData.z * sin(orientation.pitch) - magData.y * cos(orientation.pitch), \
                                      magData.x * cos(orientation.roll) + magData.y * sin(orientation.roll) \
                                      * sin(orientation.pitch) + magData.z * sin(orientation.roll) * cos(orientation.pitch)); 
          
        if (mag.getFailure()) {
          failure = true;
          LOG_ERROR << "mag error detected";
        }   
    }
    
}
/**
 * Infinite loop to read range from sonar.
 */
void IMU::updateHeight() {
    while (!stopped) {
        float range = sonar.getRange();
        if (range > 0) {
            height = range;
        }
        
        if (sonar.getFailure()) {
            failure = true;
            LOG_ERROR << "sonar error detected";
        }
    }
}

/**
 * Ask all threads to stop and wait a safe amount of time.
 */
void IMU::cleanup() {
    stopped = true;
    gpioDelay(200000);
}
