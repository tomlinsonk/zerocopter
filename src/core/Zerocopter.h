/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-09-30
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   Kiran Tomlinson
* @Last modified time: 2016-10-04
*/



#ifndef ZEROCOPTER_ZEROCOPTER_H
#define ZEROCOPTER_ZEROCOPTER_H

#include "IMU.h"
#include "Motor.h"
#include "Switch.h"
#include "LED.h"

/************************************* 
 *              PINS                 *
 *************************************/
static const int MOTOR_PIN_1 = 20;
static const int MOTOR_PIN_2 = 24;
static const int MOTOR_PIN_3 =  5;
static const int MOTOR_PIN_4 = 19;
static const int SWITCH_PIN  = 25;
static const int LED_PIN     = 18;


/**
 * The Zerocopter class represents the entire Zerocopter.
 */
class Zerocopter {
private:
    Motor motor1{MOTOR_PIN_1};
    Motor motor2{MOTOR_PIN_2};
    Motor motor3{MOTOR_PIN_3};
    Motor motor4{MOTOR_PIN_4};
    
    Switch toggle{SWITCH_PIN};
    LED led{LED_PIN};
    
    IMU imu;
    
    void abort();
    
public:
    Zerocopter();
    void fly();
    void init();
    void cleanup();
};

#endif // ZEROCOPTER_ZEROCOPTER_H