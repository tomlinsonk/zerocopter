/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-10-01
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   kiran
* @Last modified time: 2016-10-04
*/



#include "Switch.h"

#include <pigpio.h>

using namespace std;

/**
 * Constructor. Enable pull up resistor
 */
Switch::Switch(int pinNumber) {
    pin = pinNumber;
    gpioSetMode(pin, PI_INPUT);
    gpioSetPullUpDown(pin, PI_PUD_UP);
}

/**
 * Return the value of the switch
 * @return 1 or 0
 */
bool Switch::getValue() {
    return gpioRead(pin);
}