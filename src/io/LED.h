/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-09-30
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   kiran
* @Last modified time: 2016-10-04
*/



#ifndef ZEROCOPTER_LED_H
#define ZEROCOPTER_LED_H


enum LEDMode {
    LED_STATIC,
    LED_BLINK,
    LED_BURST
};


/**
 * The LED class represents an LED connected to the Zerocopter.
 */
class LED {
private:
    int pin;
    LEDMode mode;
    bool staticValue;
    unsigned short burstLength;
    unsigned int blinkPeriod;
    
    bool stopped;
    
    void LEDThread();
public:
    LED(int pinNumber);
    void set(bool onOff);
    void blink(unsigned int periodMicros);
    void burst(unsigned short length);
    void cleanup();
};

#endif // ZEROCOPTER_LED_H