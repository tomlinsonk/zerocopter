/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-09-30
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   kiran
* @Last modified time: 2016-10-04
*/



#include "LED.h"

#include <pigpio.h>

#include <thread>

using namespace std;

/**
 * Constructor. Set to off and start LED thread
 */
LED::LED(int pinNumber) {
    pin = pinNumber;
    gpioSetMode(pin, PI_OUTPUT);
    
    mode = LED_STATIC;
    staticValue = false;
    burstLength = 0;
    blinkPeriod = 0;
    stopped = false;
    
    thread runThread(&LED::LEDThread, this);
    runThread.detach();
}

/**
 * Set the LED to static mode and set the on/off value
 * @param onOff [description]
 */
void LED::set(bool onOff) {
    staticValue = onOff;
    mode = LED_STATIC;
}

/**
 * Set the LED to blink mode and update the period
 * @param periodMicros blink period in us
 */
void LED::blink(unsigned int periodMicros) {
    if (periodMicros < 100000) {
        periodMicros = 100000;
    } else if (periodMicros > 1000000) {
        periodMicros = 1000000;
    }
    
    blinkPeriod = periodMicros;
    mode = LED_BLINK;
}


/**
 * Sed the LED to burst mode and update the burst length
 * @param length the number of flashes in a burst
 */
void LED::burst(unsigned short length) {
    mode = LED_BURST;
    if (length > 5) {
        length = 5;
    }
    
    burstLength = length;
    mode = LED_BURST;
}

/**
 * Make the LED act according to its state.
 * Refreshes the LED every 1s
 */
void LED::LEDThread() {
    while (!stopped) {
        switch (mode) {
            case LED_STATIC: 
                gpioWrite(pin, staticValue);
                gpioDelay(1000000);
                break;
            case LED_BURST:
                for (int i = 0; i < burstLength; i++) {
                    gpioWrite(pin, 1);
                    gpioDelay(100000);
                    gpioWrite(pin, 0);
                    gpioDelay(100000);
                }
                gpioDelay(1000000 - (200000 * burstLength));
                break;
            case LED_BLINK:
                gpioWrite(pin, 0);
                gpioDelay(blinkPeriod / 2);
                gpioWrite(pin, 1);
                gpioDelay(blinkPeriod / 2);
                break;
        }
    }
}

/**
 * Ask the thread to stop and give it some time. Then, turn the LED off
 */
void LED::cleanup() {
    stopped = true;
    gpioDelay(1000000);
    gpioWrite(pin, 0);
}