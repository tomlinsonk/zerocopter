/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-09-30
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   kiran
* @Last modified time: 2016-10-04
*/



#ifndef ZEROCOPTER_SWITCH_H
#define ZEROCOPTER_SWITCH_H

/**
 * The Switch class represents a switch connected to the Zerocopter.
 */
class Switch {
private:
    int pin;
public:
    Switch(int pinNumber);
    bool getValue();
};

#endif // ZEROCOPTER_SWITCH_H