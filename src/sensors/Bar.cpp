/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-09-30
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   Kiran Tomlinson
* @Last modified time: 2016-10-04
*/



#include "Bar.h"

#include <pigpio.h>
#include <plog/Log.h>

#include <iostream>

using namespace std;

/**
 * Constructor. Setup I2C, set default mode, and read calibration data
 */
Bar::Bar() : failure(false)  {
    handle = i2cOpen(1, BAR_ADDRESS, 0);
    
    // Set default mode
    setMode(BAR_MODE_ULTRA_LOW_POWER);
    
    // Read factory set calibration values
    cal = readCalibrationData();
}

/**
 * Read an 8bit bar register. Throw an error if the read fails.
 * @param reg register to read from
 * @return data stored in that register
 */
int Bar::readByte(BarRegister reg) {
    int data = i2cReadByteData(handle, reg);

    if (data < 0) {
        LOG_ERROR << "Error code " << to_string(data) << ". Reading from bar register: " << to_string(reg);
        failure = true;
    }

    return data;
}

/**
 * Write to an 8bit bar register. Throw an error if the write fails.
 * @param reg register to write to from
 * @param data bits to write to register
 */
void Bar::writeByte(BarRegister reg, int data) {
    int returnCode = i2cWriteByteData(handle, reg, data);

    if (returnCode < 0) {
        LOG_ERROR << "Error code " << to_string(data) << ". Writing to bar register: " << to_string(reg);
        failure = true;
    }
}

/**
 * Read a 16bit bar register. Throw an error if the read fails.
 * @param reg register to read from
 * @return data stored in that register
 */
int Bar::readWord(BarRegister reg) {
    int data = i2cReadWordData(handle, reg);

    if (data < 0) {
        LOG_ERROR << "Error code " << to_string(data) << ". Reading word from bar register: " << to_string(reg);
        failure = true;
    }

    return ((data & 0xff) << 8) | (data >> 8);
}

/**
 * Read calibration data from the bar used to adjust raw data input.
 * @return all calibration data in a struct
 */
BarCalibrationData Bar::readCalibrationData() {
    BarCalibrationData data;
    
    data.AC1 = readWord(BAR_REGISTER_CAL_AC1);
    data.AC2 = readWord(BAR_REGISTER_CAL_AC2);
    data.AC3 = readWord(BAR_REGISTER_CAL_AC3);
    data.AC4 = readWord(BAR_REGISTER_CAL_AC4);
    data.AC5 = readWord(BAR_REGISTER_CAL_AC5);
    data.AC6 = readWord(BAR_REGISTER_CAL_AC6);
    data.B1  = readWord(BAR_REGISTER_CAL_B1);
    data.B2  = readWord(BAR_REGISTER_CAL_B2);
    data.MB  = readWord(BAR_REGISTER_CAL_MB);
    data.MC  = readWord(BAR_REGISTER_CAL_MC);
    data.MD  = readWord(BAR_REGISTER_CAL_MD);
    
    // data.AC1 = 408;
    // data.AC2 = -72;
    // data.AC3 = -14383;
    // data.AC4 = 32741;
    // data.AC5 = 32757;
    // data.AC6 = 23153;
    // data.B1  = 6190;
    // data.B2  = 4;
    // data.MB  = -32768;
    // data.MC  = -8711;
    // data.MD  = 2868;

    return data;
}

/**
 * Accessor for bar mode
 * @return the bar mode
 */
int Bar::getMode() {
    return barMode;
}

/**
 * Mutator for bar mode
 * @param mode new mode
 */
void Bar::setMode(BarMode mode) {
    barMode = mode;
}

/**
 * Read raw pressure data from the bar and convert that into a real pressure
 * measurement in Pa.
 * @return the pressure in Pa
 */
BarData Bar::getBarData() {
    // Read uncompensated temperature
    writeByte(BAR_REGISTER_CONTROL, 0x2E);
    gpioDelay(5000);
    long rawTemp = (long)readWord(BAR_REGISTER_DATA);

    // Read uncompensated pressure
    writeByte(BAR_REGISTER_CONTROL, 0x34 + (barMode << 6));
    switch (barMode) {
        case BAR_MODE_ULTRA_LOW_POWER:
            gpioDelay(5000);
            break;
        case BAR_MODE_STANDARD:
            gpioDelay(8000);
            break;
        case BAR_MODE_HIGH_RES:
            gpioDelay(14000);
            break;
        case BAR_MODE_ULTRA_HIGH_RES:
            gpioDelay(26000);
            break;
    }
    long rawPressure = (long)(readWord(BAR_REGISTER_DATA) << 8) + readByte(BAR_REGISTER_DATA_XLSB);
    rawPressure >>= 8 - barMode;

    // Calculate true temperature
    long X1 = ((rawTemp - cal.AC6) * cal.AC5) >> 15;
    long X2 = (cal.MC << 11) / (X1 + cal.MD);
    long B5 = X1 + X2;
    long temp = (B5 + 8) >> 4;
    
    // Calculate true pressure
    long B6 = B5 - 4000;
    X1 = (cal.B2 * (B6 * B6 >> 12)) >> 11;
    X2 = cal.AC2 * B6 >> 11;
    long X3 = X1 + X2;
    long B3 = (((cal.AC1 * 4 + X3) << barMode) + 2) >> 2;
    X1 = (cal.AC3 * B6) >> 13;
    X2 = (cal.B1 * (B6 * B6 >> 12)) >> 16;
    X3 = ((X1 + X2) + 2) >> 2;
    unsigned long B4 = cal.AC4 * (unsigned long)(X3 + 32768) >> 15;
    unsigned long B7 = ((unsigned long)rawPressure - B3) * (50000 >> barMode);
    long pressure;
    if (B7 < 0x80000000) {
        pressure = (B7 * 2) / B4;
    } else {
        pressure = (B7 / B4) * 2;
    }
    X1 = (pressure >> 8) * (pressure >> 8);
    X1 = (X1 * 3038) >> 16;
    X2 = (-7357 * pressure) >> 16;
    pressure = pressure + ((X1 + X2 + 3791) >> 4);
    
    BarData data;
    data.pressure = pressure;
    data.temp = temp;
    return data;    
}

/**
 * Accessor for failure
 * @return true or false
 */
bool Bar::getFailure() {
    return failure;
}
