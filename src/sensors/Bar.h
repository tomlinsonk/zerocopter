/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-09-30
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   Kiran Tomlinson
* @Last modified time: 2016-10-04
*/



#ifndef ZEROCOPTER_BAR_H
#define ZEROCOPTER_BAR_H

/*=================================================================
                           I2C DEVICE ADDRESS
 =================================================================*/
static const int BAR_ADDRESS = 0x77;

/*=================================================================
                             REGISTERS
 =================================================================*/
enum BarRegister {                           // DEFAULT    TYPE
    BAR_REGISTER_CAL_AC1              = 0xAA,  //               r
    BAR_REGISTER_CAL_AC2              = 0xAC,  //               r
    BAR_REGISTER_CAL_AC3              = 0xAE,  //               r
    BAR_REGISTER_CAL_AC4              = 0xB0,  //               r
    BAR_REGISTER_CAL_AC5              = 0xB2,  //               r
    BAR_REGISTER_CAL_AC6              = 0xB4,  //               r
    BAR_REGISTER_CAL_B1               = 0xB6,  //               r
    BAR_REGISTER_CAL_B2               = 0xB8,  //               r
    BAR_REGISTER_CAL_MB               = 0xBA,  //               r
    BAR_REGISTER_CAL_MC               = 0xBC,  //               r
    BAR_REGISTER_CAL_MD               = 0xBE,  //               r
    BAR_REGISTER_CHIP_ID              = 0xD0,
    BAR_REGISTER_VERSION              = 0xD1,
    BAR_REGISTER_SOFT_RESET           = 0xE0,
    BAR_REGISTER_CONTROL              = 0xF4,  // T=0x2E P=0x34 rw
    BAR_REGISTER_DATA                 = 0xF6,  //               r
    BAR_REGISTER_DATA_XLSB            = 0xF8   //               r
};



/*=================================================================
                          DEVICE MODE AND CALIBRATION
 =================================================================*/
enum BarMode {
    BAR_MODE_ULTRA_LOW_POWER = 0,
    BAR_MODE_STANDARD        = 1,
    BAR_MODE_HIGH_RES        = 2,
    BAR_MODE_ULTRA_HIGH_RES  = 3
};

struct BarCalibrationData {
    short          AC1;
    short          AC2;
    short          AC3;
    unsigned short AC4;
    unsigned short AC5;
    unsigned short AC6;
    short          B1;
    short          B2;
    short          MB;
    short          MC;
    short          MD;
};

struct BarData {
    long pressure;
    long temp;
};

/**
 * Bar class represents the barometer on the Adafruit 10dof.
 */
class Bar {

private:
    int handle;
    int failure;
    BarMode barMode;
    BarCalibrationData cal;
    BarCalibrationData readCalibrationData();
    
    int readByte(BarRegister reg);
    void writeByte(BarRegister reg, int data);
    int readWord(BarRegister reg);


public:
    Bar();
    
    bool getFailure();
    int getMode();
    void setMode(BarMode mode);
    BarData getBarData();
};

#endif //ZEROCOPTER_BAR_H
