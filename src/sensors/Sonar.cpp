//
// Created by Kiran Tomlinson on 9/23/16.
//

#include "Sonar.h"

#include <pigpio.h>
#include <plog/Log.h>

#include <iostream>
#include <bitset>

using namespace std;


/**
 * Constructor
 */
Sonar::Sonar() : failure(false) {
    handle = i2cOpen(1, SONAR_ADDRESS, 0);
}

/**
 * Read last range from sonar
 * @return range
 */
int Sonar::readRange() {
    int data = i2cReadWordData(handle, 0x00);
    int data2 = i2cReadWordData(handle, 0x00);
    if (data != data2){
        LOG_ERROR << "sonar mismatch: " << (bitset<16>)data << " and " << (bitset<16>)data2;
    }
    
    if (data < 0) {
        LOG_ERROR << "Error code " << to_string(data) << ". Reading from sonar";
        failure = true;
    }
    
    data = (data >> 8) | ((data & 0x7f) << 8);
    
    return data;
}

/**
 * Command the sonar to ping. Throw an error if the write fails.
 * @param data bits to write to register
 */
void Sonar::ping() {
    int returnCode = i2cWriteByte(handle, 0x51);

    if (returnCode < 0) {
        LOG_ERROR << "Error code " << to_string(returnCode) << ". Reading from sonar";
        failure = true;
    }
}

/**
 * Ping and return range reading.
 * @return distance in m
 */
float Sonar::getRange() {
    ping();
    gpioDelay(100000);
    return readRange() / 100.0;
}

/**
 * Accessor for failure
 * @return true or false
 */
bool Sonar::getFailure() {
    return failure;
}
