//
// Created by Kiran Tomlinson on 9/23/16.
//

#ifndef ZEROCOPTER_SONAR_H
#define ZEROCOPTER_SONAR_H

/*=================================================================
                           I2C DEVICE ADDRESS
 =================================================================*/
static const int SONAR_ADDRESS = 0x70;




/**
 * Sonar class represents the I2CXL MaxSonar rangefinder
 */
class Sonar {

private:
    int handle;
    bool failure;

    int readRange();
    void ping();


public:
    Sonar();
    float getRange();
    bool getFailure();
};


#endif //ZEROCOPTER_SONAR_H
