/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-09-30
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   Kiran Tomlinson
* @Last modified time: 2016-10-04
*/



#ifndef ZEROCOPTER_GYRO_H
#define ZEROCOPTER_GYRO_H

#include <map>


/*=================================================================
                           I2C DEVICE ADDRESS
 =================================================================*/
static const int GYRO_ADDRESS = 0x6B;


/*=================================================================
                             REGISTERS
 =================================================================*/
enum GyroRegister {                            // DEFAULT    TYPE
    GYRO_REGISTER_WHO_AM_I            = 0x0F,   // 11010111   r
    GYRO_REGISTER_CTRL_1              = 0x20,   // 00000111   rw
    GYRO_REGISTER_CTRL_2              = 0x21,   // 00000000   rw
    GYRO_REGISTER_CTRL_3              = 0x22,   // 00000000   rw
    GYRO_REGISTER_CTRL_4              = 0x23,   // 00000000   rw
    GYRO_REGISTER_CTRL_5              = 0x24,   // 00000000   rw
    GYRO_REGISTER_REFERENCE           = 0x25,   // 00000000   rw
    GYRO_REGISTER_OUT_TEMP            = 0x26,   //            r
    GYRO_REGISTER_STATUS              = 0x27,   //            r
    GYRO_REGISTER_OUT_X_L             = 0x28,   //            r
    GYRO_REGISTER_OUT_X_H             = 0x29,   //            r
    GYRO_REGISTER_OUT_Y_L             = 0x2A,   //            r
    GYRO_REGISTER_OUT_Y_H             = 0x2B,   //            r
    GYRO_REGISTER_OUT_Z_L             = 0x2C,   //            r
    GYRO_REGISTER_OUT_Z_H             = 0x2D,   //            r
    GYRO_REGISTER_FIFO_CTRL           = 0x2E,   // 00000000   rw
    GYRO_REGISTER_FIFO_SRC            = 0x2F,   //            r
    GYRO_REGISTER_INT_CFG             = 0x30,   // 00000000   rw
    GYRO_REGISTER_INT_SRC             = 0x31,   //            r
    GYRO_REGISTER_TSH_XH              = 0x32,   // 00000000   rw
    GYRO_REGISTER_TSH_XL              = 0x33,   // 00000000   rw
    GYRO_REGISTER_TSH_YH              = 0x34,   // 00000000   rw
    GYRO_REGISTER_TSH_YL              = 0x35,   // 00000000   rw
    GYRO_REGISTER_TSH_ZH              = 0x36,   // 00000000   rw
    GYRO_REGISTER_TSH_ZL              = 0x37,   // 00000000   rw
    GYRO_REGISTER_INT_DURATION        = 0x38,   // 00000000   rw
    GYRO_REGISTER_LOW_ODR             = 0x39    // 00000000   rw
};



/*=================================================================
                          DEVICE MODES
 =================================================================*/
enum GyroRange {
    GYRO_RANGE_245  = 0, // 245 DPS
    GYRO_RANGE_500  = 1, // 500 DPS
    GYRO_RANGE_2000 = 2  // 2000 DPS
};

enum GyroRate {
    // These require xxxxxxx1 in GYRO_REGISTER_LOW_ODR
    GYRO_RATE_12_5  = 0, // 12.5 Hz
    GYRO_RATE_25    = 1, // 25 Hz
    GYRO_RATE_50    = 2, // 50 Hz
    // These require xxxxxxx0 in GYRO_REGISTER_LOW_ODR
    // Subtract 3 from these
    GYRO_RATE_100   = 3, // 100 Hz
    GYRO_RATE_200   = 4, // 200 Hz
    GYRO_RATE_400   = 5, // 400 Hz
    GYRO_RATE_800   = 6, // 800 Hz
};



/*=================================================================
                             DATA TYPES
 =================================================================*/
struct GyroData {
    float x;
    float y;
    float z;
};


/*=================================================================
                             CONSTANTS
 =================================================================*/
static const float DPS_TO_RADS = 0.017453293;
static std::map<GyroRange, float> GYRO_SENSITIVITY = {
    {GYRO_RANGE_245, 0.00875},
    {GYRO_RANGE_500, 0.0175},
    {GYRO_RANGE_2000, 0.070}
};


/**
 * Gyro class represents the gyroscope on the Adafruit 10dof.
 */
class Gyro {

private:
    int handle;
    GyroRange gyroRange;
    bool failure;

    int readByte(GyroRegister reg);
    void writeByte(GyroRegister reg, int data);

public:
    Gyro();

    void setRate(GyroRate rate);
    void setRange(GyroRange range);
    int getRate();
    int getRange();
    bool getFailure();

    GyroData getData();
};


#endif //ZEROCOPTER_GYRO_H
