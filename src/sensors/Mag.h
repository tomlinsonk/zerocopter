/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-09-30
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   Kiran Tomlinson
* @Last modified time: 2016-10-04
*/



#ifndef ZEROCOPTER_MAG_H
#define ZEROCOPTER_MAG_H

#include <map>

/*=================================================================
                           I2C DEVICE ADDRESS
 =================================================================*/
 static const int MAG_ADDRESS = 0x1E;

/*=================================================================
                             REGISTERS
 =================================================================*/
enum MagRegister {                            // DEFAULT    TYPE
    MAG_REGISTER_CRA                  = 0x00,   // 00010000   rw
    MAG_REGISTER_CRB                  = 0x01,   // 00100000   rw
    MAG_REGISTER_MR                   = 0x02,   // 00000011   rw
    MAG_REGISTER_OUT_X_H              = 0x03,   //            r
    MAG_REGISTER_OUT_X_L              = 0x04,   //            r
    MAG_REGISTER_OUT_Z_H              = 0x05,   //            r
    MAG_REGISTER_OUT_Z_L              = 0x06,   //            r
    MAG_REGISTER_OUT_Y_H              = 0x07,   //            r
    MAG_REGISTER_OUT_Y_L              = 0x08,   //            r
    MAG_REGISTER_SR                   = 0x09,   //            r
    MAG_REGISTER_IRA                  = 0x0A,   //            r
    MAG_REGISTER_IRB                  = 0x0B,   //            r
    MAG_REGISTER_IRC                  = 0x0C,   //            r
    MAG_REGISTER_TEMP_OUT_H           = 0x31,   //            r
    MAG_REGISTER_TEMP_OUT_L           = 0x32   //            r
};



/*=================================================================
                          DEVICE MODES
 =================================================================*/
enum MagRate {
    MAG_RATE_0_7 = 0,  // 0.75 Hz
    MAG_RATE_1_5 = 1,  // 1.5 Hz
    MAG_RATE_3_0 = 2,  // 3.0 Hz
    MAG_RATE_7_5 = 3,  // 7.5 Hz
    MAG_RATE_15  = 4,  // 15 Hz
    MAG_RATE_30  = 5,  // 30 Hz
    MAG_RATE_75  = 6,  // 75 Hz
    MAG_RATE_220 = 7   // 200 Hz
};

enum MagRange {
    MAG_RANGE_1_3 = 1,  // +/- 1.3
    MAG_RANGE_1_9 = 2,  // +/- 1.9
    MAG_RANGE_2_5 = 3,  // +/- 2.5
    MAG_RANGE_4_0 = 4,  // +/- 4.0
    MAG_RANGE_4_7 = 5,  // +/- 4.7
    MAG_RANGE_5_6 = 6,  // +/- 5.6
    MAG_RANGE_8_1 = 7   // +/- 8.1
};



/*=================================================================
                             DATA TYPES
 =================================================================*/
struct MagData {
    float x;
    float y;
    float z;
};


/*=================================================================
                             CONSTANTS
 =================================================================*/

static std::map<MagRange, float> MAG_SENSITIVITY_XY = {
    {MAG_RANGE_1_3, 1100},
    {MAG_RANGE_1_9, 855},
    {MAG_RANGE_2_5, 670},
    {MAG_RANGE_4_0, 450},
    {MAG_RANGE_4_7, 400},
    {MAG_RANGE_5_6, 330},
    {MAG_RANGE_8_1, 230}
};

static std::map<MagRange, float> MAG_SENSITIVITY_Z = {
    {MAG_RANGE_1_3, 980},
    {MAG_RANGE_1_9, 760},
    {MAG_RANGE_2_5, 600},
    {MAG_RANGE_4_0, 400},
    {MAG_RANGE_4_7, 355},
    {MAG_RANGE_5_6, 295},
    {MAG_RANGE_8_1, 205}
};


/**
 * Mag class represents the magnetometer on the Adafruit 10dof.
 */
class Mag {

private:
    int handle;
    MagRange magRange;
    bool failure;
    
    int readByte(MagRegister reg);
    void writeByte(MagRegister reg, int data);

public:
    Mag();

    void setRate(MagRate rate);
    void setRange(MagRange range);
    int getRate();
    int getRange();
    bool getFailure();

    MagData getData();
};


#endif //ZEROCOPTER_MAG_H
