/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-09-30
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   Kiran Tomlinson
* @Last modified time: 2016-10-04
*/



#include "Gyro.h"

#include <pigpio.h>
#include <plog/Log.h>

#include <cstdint>
#include <iostream>

using namespace std;


/**
 * Contructor. Setup I2C and ensure we have a connection.
 */
Gyro::Gyro() : failure(false)  {
    handle = i2cOpen(1, GYRO_ADDRESS, 0);

    int whoAmI = readByte(GYRO_REGISTER_WHO_AM_I);
    if (whoAmI != 0b11010111) {
        LOG_ERROR << "GYRO_REGISTER_WHO_AM_I has value " << to_string(whoAmI) << ", expected 0b11010111";
        failure = true;
    }
    
    // Enable all 3 axes and turn chip on
    writeByte(GYRO_REGISTER_CTRL_1, 0b00001111);

    // Set lowest rate and range
    setRate(GYRO_RATE_12_5);
    setRange(GYRO_RANGE_245);
}


/**
 * Read an 8bit gyro register. Throw an error if the read fails.
 * @param reg register to read from
 * @return data stored in that register
 */
int Gyro::readByte(GyroRegister reg) {
    int data = i2cReadByteData(handle, reg);

    if (data < 0) {
        LOG_ERROR << "Error code " << to_string(data) << ". Reading from gyro register: " << to_string(reg);
        failure = true;
    }

    return data;
}

/**
 * Write to an 8bit gyro register. Throw an error if the write fails.
 * @param reg register to write to from
 * @param data bits to write to register
 */
void Gyro::writeByte(GyroRegister reg, int data) {
    int returnCode = i2cWriteByteData(handle, reg, data);

    if (returnCode < 0) {
        LOG_ERROR << "Error code " << to_string(data) << ". Writing to gyro register: " << to_string(reg);
        failure = true;
    }
}

/**
 * Accessor for data rate
 * @return current data rate
 */
int Gyro::getRate() {
    // I don't like the low odr system used by the gyro.
    int rate = (readByte(GYRO_REGISTER_CTRL_1) & 0b11000000) >> 6;
    int lowOdr = readByte(GYRO_REGISTER_LOW_ODR) & 0b00000001;

    if (lowOdr > 0) {
        return rate;
    } else {
        return rate + 3;
    }
}

/**
 * Mutator for data rate
 * @param new data rate
 */
void Gyro::setRate(GyroRate rate) {
    int newData;

    // Rate goes in left two bits
    int currData = readByte(GYRO_REGISTER_CTRL_1);
    if (rate > GYRO_RATE_50) {
        newData = (currData & 0b00111111) | ((rate - 3) << 6);
    } else {
        newData = (currData & 0b00111111) | (rate << 6);
    }
    writeByte(GYRO_REGISTER_CTRL_1, newData);

    // Low odr goes in rightmost bit
    currData = readByte(GYRO_REGISTER_LOW_ODR);
    if (rate > GYRO_RATE_50) {
        newData = currData & 0b11111110;
    } else {
        newData = currData | 1;
    }
    writeByte(GYRO_REGISTER_LOW_ODR, newData);
}

/**
 * Accessor for operating data range
 * @return current data range
 */
int Gyro::getRange() {
    return (readByte(GYRO_REGISTER_CTRL_4) & 0b00110000) >> 4;
}

/**
 * Mutator for operating data range
 * @param new data range
 */
void Gyro::setRange(GyroRange range) {
    gyroRange = range;
    int currData = readByte(GYRO_REGISTER_CTRL_4);
    int newData = (currData & 0b11001111) | (range << 4);
    writeByte(GYRO_REGISTER_CTRL_4, newData);
}

/**
 * Read data and return it in a GyroData struct of rad/s x, y, z floats
 * @return data from gyroscope
 */
GyroData Gyro::getData() {
    GyroData data;
    
    uint8_t xLo = readByte(GYRO_REGISTER_OUT_X_L);
    uint8_t xHi = readByte(GYRO_REGISTER_OUT_X_H);
    uint8_t yLo = readByte(GYRO_REGISTER_OUT_Y_L);
    uint8_t yHi = readByte(GYRO_REGISTER_OUT_Y_H);
    uint8_t zLo = readByte(GYRO_REGISTER_OUT_Z_L);
    uint8_t zHi = readByte(GYRO_REGISTER_OUT_Z_H);
    
    data.x = (int16_t)((xHi << 8) | xLo);
    data.y = (int16_t)((yHi << 8) | yLo);
    data.z = (int16_t)((zHi << 8) | zLo);
  
    data.x *= GYRO_SENSITIVITY[gyroRange] * DPS_TO_RADS;
    data.y *= GYRO_SENSITIVITY[gyroRange] * DPS_TO_RADS;
    data.z *= GYRO_SENSITIVITY[gyroRange] * DPS_TO_RADS;
  

    return data;
}


/**
 * Accessor for failure
 * @return true or false
 */
bool Gyro::getFailure() {
    return failure;
}