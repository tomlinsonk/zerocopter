/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-09-30
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   Kiran Tomlinson
* @Last modified time: 2016-10-04
*/



#ifndef ZEROCOPTER_ACCEL_H
#define ZEROCOPTER_ACCEL_H

#include <map>

/*=================================================================
                           I2C DEVICE ADDRESS
 =================================================================*/
static const int ACCEL_ADDRESS = 0x19;

/*=================================================================
                             REGISTERS
 =================================================================*/
enum AccelRegister {                           // DEFAULT    TYPE
    ACCEL_REGISTER_CTRL_1             = 0x20,   // 00000111   rw
    ACCEL_REGISTER_CTRL_2             = 0x21,   // 00000000   rw
    ACCEL_REGISTER_CTRL_3             = 0x22,   // 00000000   rw
    ACCEL_REGISTER_CTRL_4             = 0x23,   // 00000000   rw
    ACCEL_REGISTER_CTRL_5             = 0x24,   // 00000000   rw
    ACCEL_REGISTER_CTRL_6             = 0x25,   // 00000000   rw
    ACCEL_REGISTER_REFERENCE          = 0x26,   // 00000000   r
    ACCEL_REGISTER_STATUS             = 0x27,   // 00000000   r
    ACCEL_REGISTER_OUT_X_L            = 0x28,   //            r
    ACCEL_REGISTER_OUT_X_H            = 0x29,   //            r
    ACCEL_REGISTER_OUT_Y_L            = 0x2A,   //            r
    ACCEL_REGISTER_OUT_Y_H            = 0x2B,   //            r
    ACCEL_REGISTER_OUT_Z_L            = 0x2C,   //            r
    ACCEL_REGISTER_OUT_Z_H            = 0x2D,   //            r
    ACCEL_REGISTER_FIFO_CTRL          = 0x2E,   // 00000000   rw
    ACCEL_REGISTER_FIFO_SRC_REG       = 0x2F,   //            r
    ACCEL_REGISTER_INT1_CFG           = 0x30,   // 00000000   rw
    ACCEL_REGISTER_INT1_SOURCE        = 0x31,   //            r
    ACCEL_REGISTER_INT1_THS           = 0x32,   // 00000000   rw
    ACCEL_REGISTER_INT1_DURATION      = 0x33,   // 00000000   rw
    ACCEL_REGISTER_INT2_CFG           = 0x34,   // 00000000   rw
    ACCEL_REGISTER_INT2_SOURCE        = 0x35,   //            r
    ACCEL_REGISTER_INT2_THS           = 0x36,   // 00000000   rw
    ACCEL_REGISTER_INT2_DURATION      = 0x37,   // 00000000   rw
    ACCEL_REGISTER_CLICK_CFG          = 0x38,   // 00000000   rw
    ACCEL_REGISTER_CLICK_SRC          = 0x39,   // 00000000   rw
    ACCEL_REGISTER_CLICK_THS          = 0x3A,   // 00000000   rw
    ACCEL_REGISTER_TIME_LIMIT         = 0x3B,   // 00000000   rw
    ACCEL_REGISTER_TIME_LATENCY       = 0x3C,   // 00000000   rw
    ACCEL_REGISTER_TIME_WINDOW        = 0x3D   // 00000000   rw
};


/*=================================================================
                          DEVICE MODES
 =================================================================*/
enum AccelRange {
    ACCEL_RANGE_2  = 0, // +/- 2g
    ACCEL_RANGE_4  = 1, // +/- 4g
    ACCEL_RANGE_8  = 2, // +/- 8g
    ACCEL_RANGE_16 = 3  // +/- 16g
};

enum AccelRate {
    ACCEL_RATE_1     = 1, // 1 Hz
    ACCEL_RATE_10    = 2, // 10 Hz
    ACCEL_RATE_25    = 3, // 25 Hz
    ACCEL_RATE_50    = 4, // 50 Hz
    ACCEL_RATE_100   = 5, // 100 Hz
    ACCEL_RATE_200   = 6, // 200 Hz
    ACCEL_RATE_400   = 7, // 400 Hz
    ACCEL_RATE_1_3_k = 9  // 1.3 kHz
};



/*=================================================================
                             DATA TYPES
 =================================================================*/
struct AccelData {
    float x;
    float y;
    float z;
};

/*=================================================================
                             CONSTANTS
 =================================================================*/
 static std::map<AccelRange, float> ACCEL_SENSITIVITY = {
     {ACCEL_RANGE_2, 0.001},
     {ACCEL_RANGE_4, 0.002},
     {ACCEL_RANGE_8, 0.004},
     {ACCEL_RANGE_16, 0.012}
 };



/**
 * Accel class represents the accelerometer on the Adafruit 10dof.
 */
class Accel {

private:
    bool failure;
    int handle;
    AccelRange accelRange;
    
    int readByte(AccelRegister reg);
    void writeByte(AccelRegister reg, int data);

public:
    Accel();

    void setRate(AccelRate rate);
    void setRange(AccelRange range);
    int getRate();
    int getRange();
    bool getFailure();

    AccelData getData();
};

#endif //ZEROCOPTER_ACCEL_H
