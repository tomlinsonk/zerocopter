/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-09-30
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   Kiran Tomlinson
* @Last modified time: 2016-10-04
*/



#include "Accel.h"

#include <pigpio.h>

#include <cstdint>
#include <iostream>
#include <plog/Log.h>

using namespace std;

/**
 * Constructor. Initialize I2c, check connection, and set default rate and range.
 */
Accel::Accel() : failure(false)  {
    handle = i2cOpen(1, ACCEL_ADDRESS, 0);
    
    // Check connection to accel by writing/reading register
    writeByte(ACCEL_REGISTER_CTRL_1, 0b00000111);
    int regCheck = readByte(ACCEL_REGISTER_CTRL_1);
    if (regCheck != 0b00000111) {
        LOG_ERROR << "accel register test failed: got " << regCheck << ", expected 0b00000111";
        failure = true;
    }

    // Set lowest rate and range
    setRate(ACCEL_RATE_1);
    setRange(ACCEL_RANGE_2);
}

/**
 * Read an 8bit accel register. Throw an error if the read fails.
 * @param reg register to read from
 * @return data stored in that register
 */
int Accel::readByte(AccelRegister reg) {
    int data = i2cReadByteData(handle, reg);

    if (data < 0) {
        LOG_ERROR << "Error code " << to_string(data) << ". Reading from accel register: " << to_string(reg);
        failure = true;
    }

    return data;
}

/**
 * Write to an 8bit accel register. Throw an error if the write fails.
 * @param reg register to write to from
 * @param data bits to write to register
 */
void Accel::writeByte(AccelRegister reg, int data) {
    int returnCode = i2cWriteByteData(handle, reg, data);

    if (returnCode < 0) {
        LOG_ERROR << "Error code " << to_string(data) << ". Writing to accel register: " << to_string(reg);
        failure = true;
    }
}

/**
 * Accessor for accel data rate
 * @return current data rate
 */
int Accel::getRate() {
    return (readByte(ACCEL_REGISTER_CTRL_1) & 0b11110000) >> 4;
}

/**
 * Mutator for accel data rate
 * @param rate new data rate
 */
void Accel::setRate(AccelRate rate) {
    int currData = readByte(ACCEL_REGISTER_CTRL_1);
    int newData = (currData & 0b00001111) | (rate << 4);
    writeByte(ACCEL_REGISTER_CTRL_1, newData);
}

/**
 * Accessor for accel range
 * @return accel range
 */
int Accel::getRange() {
    return (readByte(ACCEL_REGISTER_CTRL_4) & 0b00110000) >> 4;
}

/**
 * Mutator for accel range
 * @param range new range
 */
void Accel::setRange(AccelRange range) {
    accelRange = range;
    int currData = readByte(ACCEL_REGISTER_CTRL_4);
    int newData = (currData & 0b11001111) | (range << 4);
    writeByte(ACCEL_REGISTER_CTRL_4, newData);
}

AccelData Accel::getData() {
    AccelData data;
    
    uint8_t xLo = readByte(ACCEL_REGISTER_OUT_X_L);
    uint8_t xHi = readByte(ACCEL_REGISTER_OUT_X_H);
    uint8_t yLo = readByte(ACCEL_REGISTER_OUT_Y_L);
    uint8_t yHi = readByte(ACCEL_REGISTER_OUT_Y_H);
    uint8_t zLo = readByte(ACCEL_REGISTER_OUT_Z_L);
    uint8_t zHi = readByte(ACCEL_REGISTER_OUT_Z_H);
    
    data.x = (int16_t)((xHi << 8) | xLo);
    data.y = (int16_t)((yHi << 8) | yLo);
    data.z = (int16_t)((zHi << 8) | zLo);
      
    data.x *= ACCEL_SENSITIVITY[accelRange];
    data.y *= ACCEL_SENSITIVITY[accelRange];
    data.z *= ACCEL_SENSITIVITY[accelRange];
    
    return data;
}

/**
 * Accessor for failure
 * @return true or false
 */
bool Accel::getFailure() {
    return failure;
}
