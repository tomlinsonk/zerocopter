/**
* @Author: Kiran Tomlinson <kiran>
* @Date:   2016-09-30
* @Email:  kirantomlinson@gmail.com
* @Project: Zerocopter
* @Last modified by:   Kiran Tomlinson
* @Last modified time: 2016-10-04
*/



#include "Mag.h"

#include <pigpio.h>
#include <plog/Log.h>

#include <iostream>


using namespace std;

/**
 * Constructor. Setup I2c, test connection, and set default values.
 */
Mag::Mag() : failure(false) {
    handle = i2cOpen(1, MAG_ADDRESS, 0);
    
    // Check connection to mag by reading register
    int regCheck = readByte(MAG_REGISTER_IRA);
    if (regCheck != 0b01001000) {
        LOG_ERROR << "MAG_REGISTER_IRA has value " << to_string(regCheck) << ", expected 0b01001000";
        failure = true;
    }
    
    // Enable mag
    writeByte(MAG_REGISTER_MR, 0x00);

    // Set lowest rate and range
    setRate(MAG_RATE_0_7);
    setRange(MAG_RANGE_1_3);
}

/**
 * Read an 8bit mag register. Throw an error if the read fails.
 * @param reg register to read from
 * @return data stored in that register
 */
int Mag::readByte(MagRegister reg) {
    int data = i2cReadByteData(handle, reg);

    if (data < 0) {
        LOG_ERROR << "Error code " << to_string(data) << ". Reading from mag register: " << to_string(reg);
        failure = true;
    }

    return data;
}

/**
 * Write to an 8bit mag register. Throw an error if the write fails.
 * @param reg register to write to from
 * @param data bits to write to register
 */
void Mag::writeByte(MagRegister reg, int data) {
    int returnCode = i2cWriteByteData(handle, reg, data);

    if (returnCode < 0) {
        LOG_ERROR << "Error code " << to_string(data) << ". Writing to mag register: " << to_string(reg);
        failure = true;
    }
}



/**
 * Accessor for mag data rate
 * @return current data rate
 */
int Mag::getRate() {
    return readByte(MAG_REGISTER_CRA) >> 2;
}

/**
 * Mutator for mag data rate
 * @param rate new data rate
 */
void Mag::setRate(MagRate rate) {
    int newData = rate << 2;
    writeByte(MAG_REGISTER_CRA, newData);
}

/**
 * Acessor for mag data range
 * @return current data range
 */
int Mag::getRange() {
    return readByte(MAG_REGISTER_CRB) >> 5;
}

/**
 * Mutator for mag data range
 * @param range new data range
 */
void Mag::setRange(MagRange range) {
    magRange = range;
    int newData = range << 5;
    writeByte(MAG_REGISTER_CRB, newData);
}


/**
 * Read mag data and return it in a MagData struct
 * @return mag data x, y, z
 */
MagData Mag::getData() {
    MagData data;
    
    uint8_t xLo = readByte(MAG_REGISTER_OUT_X_L);
    uint8_t xHi = readByte(MAG_REGISTER_OUT_X_H);
    uint8_t yLo = readByte(MAG_REGISTER_OUT_Y_L);
    uint8_t yHi = readByte(MAG_REGISTER_OUT_Y_H);
    uint8_t zLo = readByte(MAG_REGISTER_OUT_Z_L);
    uint8_t zHi = readByte(MAG_REGISTER_OUT_Z_H);
    
    data.x = (int16_t)((xHi << 8) | xLo);
    data.y = (int16_t)((yHi << 8) | yLo);
    data.z = (int16_t)((zHi << 8) | zLo);
      
    data.x *= MAG_SENSITIVITY_XY[magRange];
    data.y *= MAG_SENSITIVITY_XY[magRange];
    data.z *= MAG_SENSITIVITY_Z[magRange];

    return data;
}


/**
 * Accessor for failure
 * @return true or false
 */
bool Mag::getFailure() {
    return failure;
}
